﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace TomCafe
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        List<MenuItem> beverageList = new List<MenuItem>();
        List<MenuItem> sidesList = new List<MenuItem>();
        List<MenuItem> bundle = new List<MenuItem>();
        List<ValueMeal> valuemeal = new List<ValueMeal>();
        List<Beverage> drinksList = new List<Beverage>();
        MenuItem original = new MenuItem();


        DateTime d1 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 7, 0, 0);
        DateTime d2 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 14, 0, 0);
        DateTime d3 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 10, 0, 0);
        DateTime d4 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 19, 0, 0);
        DateTime d5 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 16, 0, 0);
        DateTime d6 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 22, 0, 0);
        DateTime d7 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
        DateTime d8 = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);
        int select = 0;
        Order order = new Order();

        public MainPage()
        {
            this.InitializeComponent();

            order.OrderNo = 1;
        }

        private void init()
        {
            DateTime d = DateTime.Now;

            sidesList.Clear();
            beverageList.Clear();
            bundle.Clear();
            valuemeal.Clear();

            ValueMeal v1 = new ValueMeal("Hotcake with Sausage", 6.90, d1, d2);
            ValueMeal v2 = new ValueMeal("Hamburger", 7.50, d3, d4);
            ValueMeal v3 = new ValueMeal("Ribeye steak", 10.20, d5, d6);
            ValueMeal v4 = new ValueMeal("Nasi Lemak", 5.40, d7, d8);

            MenuItem m1 = new MenuItem("Breakfast set", 7.90);
            m1.ProductList.Add(new ValueMeal("Hotcake with Sausage", 6.90, d1, d2));
            m1.ProductList.Add(new Sides("Hash brown", 2.10));

            MenuItem m2 = new MenuItem("Hamburger Combo", 10.20);
            m2.ProductList.Add(new ValueMeal("Hamburger", 7.50, d3, d4));
            m2.ProductList.Add(new Sides("Truffle Fries", 3.70));
            m2.ProductList.Add(new Beverage("Cola", 2.85, 0.0));

            MenuItem m3 = new MenuItem("Dinner Set", 18.50);
            m3.ProductList.Add(new ValueMeal("Ribeye steak", 10.20, d5, d6));
            m3.ProductList.Add(new Sides("Truffle Fries", 3.70));
            m3.ProductList.Add(new Sides("Caesar salad", 4.30));
            m3.ProductList.Add(new Beverage("Coffee", 2.70, 0.0));

            sidesList.Add(new MenuItem("Hash brown", 2.10));
            sidesList.Add(new MenuItem("Truffle fries", 3.70));
            sidesList.Add(new MenuItem("Calamari\t", 3.40));
            sidesList.Add(new MenuItem("Caesar salad", 4.30));

            beverageList.Add(new MenuItem("Cola\t", 2.85));
            beverageList.Add(new MenuItem("Green Tea", 3.70));
            beverageList.Add(new MenuItem("Coffee\t", 2.70));
            beverageList.Add(new MenuItem("Tea\t", 2.70));
            beverageList.Add(new MenuItem("Tom's Root Beer", 9.70));
            beverageList.Add(new MenuItem("Mocktail\t", 15.90));

            if (d.Hour >= d1.Hour && d.Hour < d3.Hour)
            {
                bundle.Add(m1);
                valuemeal.Add(v1);
                valuemeal.Add(v4);
            }
            else if (d.Hour >= d3.Hour && d.Hour < d2.Hour)
            {
                bundle.Add(m1);
                bundle.Add(m2);
                valuemeal.Add(v1);
                valuemeal.Add(v2);
                valuemeal.Add(v4);
            }
            else if (d.Hour >= d2.Hour && d.Hour < d5.Hour)
            {
                bundle.Add(m2);
                valuemeal.Add(v2);
                valuemeal.Add(v4);
            }
            else if (d.Hour >= d5.Hour && d.Hour < d4.Hour)
            {
                bundle.Add(m2);
                bundle.Add(m3);
                valuemeal.Add(v2);
                valuemeal.Add(v3);
                valuemeal.Add(v4);
            }
            else if (d.Hour >= d4.Hour && d.Hour < d6.Hour)
            {
                bundle.Add(m3);
                valuemeal.Add(v3);
                valuemeal.Add(v4);
            }
            else
            {
                valuemeal.Add(v4);
            }
        }

        private void mainsButton_Click(object sender, RoutedEventArgs e)
        {

            select = 1;
            init();
            itemsListView.ItemsSource = null;
            itemsListView.ItemsSource = valuemeal;
        }

        private void sidesButton_Click(object sender, RoutedEventArgs e)
        {
            select = 2;
            itemsListView.ItemsSource = null;
            itemsListView.ItemsSource = sidesList;
        }

        private void beveragesButton_Click(object sender, RoutedEventArgs e)
        {
            select = 3;
            itemsListView.ItemsSource = null;
            itemsListView.ItemsSource = beverageList;
        }

        private void bundlesButton_Click(object sender, RoutedEventArgs e)
        {
            select = 4;
            init();
            itemsListView.ItemsSource = null;
            itemsListView.ItemsSource = bundle;
        }

        private void addButton_Click(object sender, RoutedEventArgs e)
        {
            if (itemsListView.SelectedIndex != -1)
            {
                if (select == 1)
                {
                    Product p = (Product)itemsListView.SelectedItem;
                    MenuItem x = new MenuItem();
                    x.Name = p.Name;
                    x.Price = p.Price;
                    order.Add(new OrderItem(x));
                    cartsListView.ItemsSource = null;
                    cartsListView.ItemsSource = order.ItemList;
                    displayText.Text = $"{x.Name} Added.\nTotal: ${order.GetTotalAmt():0.00}\n\nWelcome to Tom's Cafe\n\nChoose your item from the menu";
                }
                else if (select == 2)
                {
                    MenuItem m = (MenuItem)itemsListView.SelectedItem;
                    order.Add(new OrderItem(m));
                    cartsListView.ItemsSource = null;
                    cartsListView.ItemsSource = order.ItemList;
                    displayText.Text = $"{m.Name} Added.\nTotal: ${order.GetTotalAmt():0.00}\n\nWelcome to Tom's Cafe\n\nChoose your item from the menu";
                }
                else if (select == 3)
                {
                    MenuItem m = (MenuItem)itemsListView.SelectedItem;
                    order.Add(new OrderItem(m));
                    cartsListView.ItemsSource = null;
                    cartsListView.ItemsSource = order.ItemList;
                    displayText.Text = $"{m.Name} Added.\n\nTotal: ${order.GetTotalAmt():0.00}\n\nWelcome to Tom's Cafe\n\nChoose your item from the menu";
                }
                else if (select == 4)
                {
                    drinksList.Clear();
                    foreach (MenuItem m in bundle)
                    {
                        if (m.ToString() == itemsListView.SelectedItem.ToString())
                        {
                            original = m;
                            if (m.Name == "Hamburger Combo")
                            {
                                drinksList.Add(new Beverage("Cola", 2.85, 0.00));
                                drinksList.Add(new Beverage("Green Tea", 3.70, 0.85));
                                drinksList.Add(new Beverage("Coffee", 2.70, 0.00));
                                drinksList.Add(new Beverage("Tea", 2.70, 0.00));
                                drinksList.Add(new Beverage("Tom's Root Beer", 9.70, 6.85));
                                drinksList.Add(new Beverage("Mocktail", 15.90, 13.05));
                            }
                            else if (m.Name == "Dinner Set")
                            {
                                drinksList.Add(new Beverage("Cola", 2.85, 0.15));
                                drinksList.Add(new Beverage("Green Tea", 3.70, 1.00));
                                drinksList.Add(new Beverage("Coffee", 2.70, 0.00));
                                drinksList.Add(new Beverage("Tea", 2.70, 0.00));
                                drinksList.Add(new Beverage("Tom's Root Beer", 9.70, 7.00));
                                drinksList.Add(new Beverage("Mocktail", 15.90, 13.20));
                            }
                        }
                    }

                    select = 5;

                    if (itemsListView.SelectedItem is MenuItem)
                    {
                        MenuItem addons = (MenuItem)itemsListView.SelectedItem;
                        if (addons.ProductList[addons.ProductList.Count() - 1] is Beverage && addons.ProductList.Count() > 1)
                        {
                            itemsListView.ItemsSource = null;
                            itemsListView.ItemsSource = drinksList;
                            original.Name = addons.Name;
                            original.Price = addons.Price;
                            original.ProductList = new List<Product>();
                            foreach (Product p in addons.ProductList)
                            {
                                original.ProductList.Add(p);
                            }
                        }
                        else
                        {
                            bool exist = false;
                            OrderItem newItem = new OrderItem(addons);
                            foreach (OrderItem oi in order.ItemList)
                            {
                                if (oi.Item == newItem.Item)
                                {
                                    exist = true;
                                    oi.AddQty();
                                    break;
                                }
                                else
                                {
                                    exist = false;
                                }
                            }
                            if (exist == false)
                            {
                                newItem.Quantity = 1;
                                order.Add(newItem);
                            }
                            displayText.Text = $"{newItem.Item.Name} has been added.\n\nTotal: ${order.GetTotalAmt():0.00}\n\nWelcome to Tom's Cafe\n\nChoose your item from the menu";
                            cartsListView.ItemsSource = null;
                            cartsListView.ItemsSource = order.ItemList;
                        }
                    }
                }
                else if (select == 5)
                {
                    if (original.Name == "Hamburger Combo")
                    {
                        original.ProductList.Add(new ValueMeal("Hamburger", 7.50, d3, d4));
                        original.ProductList.Add(new Sides("Truffle Fries", 3.70));
                        original.ProductList.Add(new Beverage("Cola", 2.85, 0.0));
                    }
                    else if (original.Name == "Dinner Set")
                    {
                        original.ProductList.Add(new ValueMeal("Ribeye steak", 10.20, d5, d6));
                        original.ProductList.Add(new Sides("Truffle Fries", 3.70));
                        original.ProductList.Add(new Sides("Caesar salad", 4.30));
                        original.ProductList.Add(new Beverage("Coffee", 2.70, 0.0));
                    }
                    MenuItem clone = new MenuItem();
                    clone.Name = original.Name;
                    clone.Price = original.Price;
                    clone.ProductList = new List<Product>();
                    for (int i = 0; i < original.ProductList.Count; i++)
                    {
                        clone.ProductList.Add(original.ProductList[i]);
                    }

                    Beverage beveragetoAdd = drinksList[itemsListView.SelectedIndex] as Beverage;
                    OrderItem addons = new OrderItem(clone);
                    addons.Item.Price += beveragetoAdd.TradeIn;
                    addons.Item.ProductList.Remove(addons.Item.ProductList[addons.Item.ProductList.Count() - 1]);
                    addons.Item.ProductList.Add(beveragetoAdd);

                    bool exist = false;
                    foreach (OrderItem i in order.ItemList)
                    {
                        if (i.Item.ToString() == addons.Item.ToString())
                        {
                            exist = true;
                            i.AddQty();
                            break;
                        }
                        else
                        {
                            exist = false;
                        }
                    }
                    if (exist == false)
                    {
                        addons.Quantity = 1;
                        order.ItemList.Add(addons);
                    }
                    cartsListView.ItemsSource = null;
                    cartsListView.ItemsSource = order.ItemList;
                    itemsListView.ItemsSource = null;
                    displayText.Text = $"{addons.Item.Name} has been added.\n\nTotal: ${order.GetTotalAmt():0.00}\n\nWelcome to Tom's Cafe\n\nChoose your item from the menu";
                    select = 4;
                }
            }
            else
            {
                displayText.Text = "Please select an item from the menu";
            }
        }
        private void orderButton_Click(object sender, RoutedEventArgs e)
        {
            cartsListView.ItemsSource = null;
            itemsListView.ItemsSource = null;
            if (order.ItemList.Count == 0)
            {
                displayText.Text = "Your cart is empty";
            }
            else
            {
                displayText.Text = $"Toms's Cafe Receipt {order.OrderNo}\n\n{DateTime.Now}\n\n";
                order.OrderNo += 1;
                foreach(OrderItem x in order.ItemList)
                {
                    if (x.Item.Name == "Hamburger Combo")
                    {
                        displayText.Text += $"{x.Quantity}\t{x.Item.Name}\t${x.GetItemTotalAmt():0.00}\n";
                        displayText.Text += $"\t{x.Item.ProductList[0].Name}\n\t{x.Item.ProductList[1].Name}\n\t{x.Item.ProductList[2].Name}\n";
                    }
                    else if (x.Item.Name == "Dinner Set")
                    {
                        displayText.Text += $"{x.Quantity}\t{x.Item.Name}\t\t${x.GetItemTotalAmt():0.00}\n";
                        displayText.Text += $"\t{x.Item.ProductList[0].Name}\n\t{x.Item.ProductList[1].Name}\n\t{x.Item.ProductList[2].Name}\n\t{x.Item.ProductList[3].Name}\n";
                    }
                    else
                    {
                        displayText.Text += $"{x.Quantity}\t{x.Item.Name}\t\t${x.GetItemTotalAmt():0.00}\n";
                    }
                }
                displayText.Text += $"\nTotal: \t\t\t\t${order.GetTotalAmt():0.00}";
            }
            order.ItemList.Clear();
            cartsListView.ItemsSource = null;
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            cartsListView.ItemsSource = null;
            cartsListView.Items.Clear();
            order.ItemList.Clear();
            itemsListView.ItemsSource = null;
            itemsListView.ItemsSource = bundle;
            displayText.Text = "Your order is cancelled." + "\n\n" + "Welcome to Tom's Cafe" + "\n\n" + "Choose your item from the menu.";
            
        }

        private void removeButton_Click(object sender, RoutedEventArgs e)
        {
            OrderItem remove = (OrderItem)cartsListView.SelectedItem;

            if (cartsListView.SelectedIndex != -1)
            {

                if (remove.RemoveQty() == true)
                {
                    cartsListView.ItemsSource = null;
                    cartsListView.ItemsSource = order.ItemList;
                    remove.Quantity = remove.Quantity - 1;
                    displayText.Text = remove.Item.Name + " has been removed." + "\n\n" + "Total: $" + order.GetTotalAmt() +
                        "\n\n" + "Welcome to Tom's Cafe" + "\n\n" + "Choose your item from the menu.";
                }
                else if (remove.Quantity == 1)
                {
                    cartsListView.ItemsSource = null;
                    order.ItemList.Remove(remove);
                    displayText.Text = remove.Item.Name + " has been removed " + "\n\n" + "Welcome to Tom's Cafe" + "\n\n" + "Choose your item from the menu.";
                    cartsListView.ItemsSource = order.ItemList;
                }
            }
            else
            {
                displayText.Text = "Please select an item from your cart to be removed";
            }

        }
    }
}
