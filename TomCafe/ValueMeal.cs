﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TomCafe
{
    class ValueMeal : Product
    {
      
        private DateTime startTime;

        public DateTime StartTime
        {
            get { return startTime; }
            set { startTime = value; }
        }
        private DateTime endTime;

        public DateTime EndTime
        {
            get { return endTime; }
            set { endTime = value; }
        }
        public ValueMeal() { }
        public ValueMeal(string n, double p, DateTime st, DateTime et) : base(n, p)
        {
            startTime = st;
            endTime = et;
        }
        public override double GetPrice()
        {
            return Price;
        }
        public bool IsAvailable()
        {
            DateTime dt = DateTime.Now;
            dt.ToString("H:mm");
            if (dt >= startTime && dt < endTime)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        public override string ToString()
        {
            return string.Format("{0}\n${1:0.00}", Name, Price);
        }

    }
}
