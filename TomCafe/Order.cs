﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TomCafe
{
    class Order
    {
        private int orderNo;

        public int OrderNo
        {
            get { return orderNo; }
            set { orderNo = value; }
        }
        private List<OrderItem> itemList = new List<OrderItem>();

        public List<OrderItem> ItemList
        {
            get { return itemList; }
            set { itemList = value; }
        }
        public Order () {}
        public void Add(OrderItem m)
        {
            bool test = false;

            foreach (OrderItem o in itemList)
            {
                if (o.Item.Name == m.Item.Name)
                {
                    test = true;
                    o.AddQty();
                    break;
                }
            }
            if (test == false)
            {
                m.Quantity = 1;
                itemList.Add(m);
            }
        }
        public void Remove(int r)
        {
            itemList.RemoveAt(r);
        }
        public double GetTotalAmt()
        {
            List<double> priceList = new List<double>();
            foreach (OrderItem o in itemList)
            {
                double stuff = o.GetItemTotalAmt();
                priceList.Add(stuff);
                
            }
            double total = priceList.Sum();
            return total;
        }
        public override string ToString()
        {
            return "";
        }
    }
    







}
