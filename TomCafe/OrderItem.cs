﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TomCafe
{
    class OrderItem
    {
        private int quantity;

        public int Quantity
        {
            get { return quantity; }
            set { quantity = value; }
        }
        private MenuItem item;

        public MenuItem Item
        {
            get { return item; }
            set { item = value; }
        }
        public OrderItem() { }
        public OrderItem(MenuItem m)
        {
            item = m;
        }
        public void AddQty()
        {
            quantity++;
        }
        public bool RemoveQty()
        {
            if (quantity > 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public double GetItemTotalAmt()
        {
                return item.Price* quantity;
        }
        public override string ToString()
        {
            if (item.ProductList.Count == 0)
            {
                return string.Format("{0}\t\tx{1}\n${2:0.00}", item.Name, quantity, GetItemTotalAmt());
            }
            for (int i = 0; i < item.ProductList.Count; i++)
            {
                if (item.Name == "Hamburger Combo")
                {
                    return $"{item.Name}\tx{quantity}\n({item.ProductList[i].Name}, {item.ProductList[i + 1].Name}, {item.ProductList[i + 2].Name})\n${item.Price}";
                }
                else if (item.Name == "Breakfast set")
                {
                    return $"{item.Name}\t\tx{quantity}\n({item.ProductList[i].Name}, {item.ProductList[i + 1].Name})\n${item.Price}";
                }
                else if (item.Name == "Dinner Set")
                {
                    return $"{item.Name}\t\tx{quantity}\n({item.ProductList[i].Name}, {item.ProductList[i + 1].Name}, {item.ProductList[i + 2].Name}, {item.ProductList[i + 3].Name})\n${item.Price}";
                }
            }
            return ToString();
        }


    }
}
